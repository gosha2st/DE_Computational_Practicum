#DE Computational Practicum by Georgiy Stepanov @gosha2st BS18-04 20191112
#Auxiliary module with computational functions:
#Original function: y'=f(x,y)=2x+y-3;
#Exact solution;
#Euler method (simplest first-order method, where local error is proportional to the square of step size)
#Improved (modified) Euler's method aka Heun's method (or explicit trapezoidal rule), which is two-stage Runge–Kutta method;
#Runge–Kutta classic fourth-order method aka RK4;
#and auxiliary computing functions for described methods (like delta and coefficients for RK).

from math import e   #Euler's number, that appears in the exact solution of the equation

#Original function: y'=2x+y-3
def origFunc(x,y):
    return 2*x+y-3

#Exact solution:
def exactSolution(x, x0 = 1, y0 = 1):
    c = (2*x0 + y0 - 1) * e**(-x0)
    return 1 + c*e**x - 2*x


#First coefficient for Runge-Kutta method:
def k1(x,y):
    return origFunc(x, y)

#Second coefficient for RK:
def k2(x,y,h):
    return origFunc(x+h/2, y+h*k1(x, y)/2)

#Third coefficient for RK:
def k3(x,y,h):
    return origFunc(x+h/2, y+h*k2(x,y,h)/2)

#Forth coefficient for RK:
def k4(x,y,h):
    return origFunc(x+h, y+h*k3(x,y,h))

#delta for Improved Euler's method:
def deltaIE(x,y,h):
    return h*origFunc(x+h/2, y+origFunc(x, y)*h/2)

#delta for Runge-Kutta method:
def deltaRK(x,y,h):
    return h * (k1(x,y) + 2*k2(x,y,h) + 2*k3(x,y,h) + k4(x,y,h)) /6


#Exact solution graph function:
def Exact(steps, xm=7, x0 = 1, y0 = 1):    
    #Initial values:
    x=[x0]
    y=[]    #intentionally blank
    h=(xm-x[0])/steps   #step size
    for i in range(steps):
        x.append(x[i]+h)
        y.append(exactSolution(x[i],x0,y0))
    y.append(exactSolution(x[-1],x0,y0))
    return x, y

#Euler method graph function:
def Euler(steps, xm=7, x0 = 1, y0 = 1):
    #Initial values:
    x=[x0]
    y=[y0]
    h=(xm-x[0])/steps   #step size
    for i in range(steps):
        x.append(x[i]+h)
        y.append(y[i]+h*(origFunc(x[i],y[i])))
    return x, y

#Improved Euler method graph function:
def ImprovedEuler(steps, xm=7, x0 = 1, y0 = 1):
    #Initial values:
    x=[x0]
    y=[y0]
    h=(xm-x[0])/steps   #step size
    for i in range(steps):
        x.append(x[i]+h)
        y.append(y[i]+deltaIE(x[i],y[i],h))
    return x, y

#Runge-Kutta method graph function:
def RungeKutta(steps, xm=7, x0 = 1, y0 = 1):
    #Initial values:
    x=[x0]
    y=[y0]
    h=(xm-x[0])/steps   #step size
    for i in range(steps):
        x.append(x[i]+h)
        y.append(y[i]+deltaRK(x[i],y[i],h))
    return x, y
