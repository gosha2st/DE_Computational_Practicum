#DE Computational Practicum by Georgiy Stepanov @gosha2st BS18-04 20191114
#variant 19: y'=2x+y-3
#CompFunction.py file in the same directory with the main program is required.
#Screen resolution must be at least 1920x1080 for a proper work;
#otherwise the appearance of all User Interface's elements is not guaranteed.

import sys
from CompFunctions import *
from math import fabs
from time import sleep
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5.QtGui import QFont
from PyQt5.QtCore import pyqtSlot, Qt, QRect
from PyQt5.QtWidgets import QApplication, QMainWindow, QSizePolicy, QPushButton, QLineEdit, QLabel

class App(QMainWindow):
    def __init__(self):     #setting main window properties:
        super().__init__()
        self.left=5
        self.top=30
        self.title='DE Computational Practicum by Georgiy Stepanov'
        self.width=1910
        self.height=1020
        self.setFixedSize(1910,1020)
        self.initUI()
    
    def initUI(self):   #initializes main user interface:
        #initializing main window:
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        m=PlotCanvas(self, width=14.5, height=11.5)   #sizes: width and height
        m.move(-50,-75)                                            #initial position: x and y
        
        #Label with title:
        self.label_title=QLabel(self)
        self.label_title.setText('DE Computational Practicum')
        self.label_title.setFixedWidth(500)
        self.label_title.setFont(QFont('Sans Serif',24,QFont.Bold))
        self.label_title.move(1444,125)
        
        #Label with name:
        self.label_name=QLabel(self)
        self.label_name.setText('by Georgiy Stepanov @gosha2st')
        self.label_name.setFixedWidth(500)
        self.label_name.setFont(QFont('Sans Serif',18))
        self.label_name.move(1475,175)
        
        #Label with additional info:
        self.label_date=QLabel(self)
        self.label_date.setText('BS18-04, date:20191114')
        self.label_date.setFixedWidth(400)
        self.label_date.setFont(QFont('Sans Serif',16))
        self.label_date.move(1540,220)
        
        #Label with variant:
        self.label_var=QLabel(self)
        self.label_var.setText('Variant 19:')
        self.label_var.setFixedWidth(450)
        self.label_var.setFont(QFont('Sans Serif',18))
        self.label_var.move(1465,305)

        #Label with initial function:
        self.label_func=QLabel(self)
        self.label_func.setText("y' = 2x + y - 3")
        self.label_func.setFixedWidth(450)
        self.label_func.setFont(QFont('Courier New',18,QFont.Bold))
        self.label_func.move(1645,305)
        
        #Textbox for x0:
        self.textbox_x0=QLineEdit(self)
        self.textbox_x0.move(1465,370)
        self.textbox_x0.resize(110,50)
        self.textbox_x0.setText('1')
        self.textbox_x0.setAlignment(Qt.AlignCenter)
        self.textbox_x0.setFont(QFont('Courier New',20,QFont.Bold))
        
        #Label for x0:
        self.label_x0=QLabel(self)
        self.label_x0.setText('x0 (x from -> )')
        self.label_x0.setFixedWidth(400)
        self.label_x0.setFont(QFont('Sans Serif',20))
        self.label_x0.move(1600,380)

        #Textbox for y0:
        self.textbox_y0=QLineEdit(self)
        self.textbox_y0.move(1465,455)
        self.textbox_y0.resize(110,50)
        self.textbox_y0.setText('1')
        self.textbox_y0.setAlignment(Qt.AlignCenter)
        self.textbox_y0.setFont(QFont('Courier New',20,QFont.Bold))

        #Label for y0:
        self.label_y0=QLabel(self)
        self.label_y0.setText('y0 (value at x0)')
        self.label_y0.setFixedWidth(400)
        self.label_y0.setFont(QFont('Sans Serif',20))
        self.label_y0.move(1600,465)

        #Textbox for X:
        self.textbox_X=QLineEdit(self)
        self.textbox_X.move(1465,540)
        self.textbox_X.resize(110,50)
        self.textbox_X.setText('7')
        self.textbox_X.setAlignment(Qt.AlignCenter)
        self.textbox_X.setFont(QFont('Courier New',20,QFont.Bold))

        #Label for X:
        self.label_X=QLabel(self)
        self.label_X.setText('X (x -> to)')
        self.label_X.setFixedWidth(400)
        self.label_X.setFont(QFont('Sans Serif',20))
        self.label_X.move(1600,550)

        #Textbox for N(steps):
        self.textbox_N=QLineEdit(self)
        self.textbox_N.move(1465,625)
        self.textbox_N.resize(110,50)
        self.textbox_N.setText('25')
        self.textbox_N.setAlignment(Qt.AlignCenter)
        self.textbox_N.setFont(QFont('Courier New',20,QFont.Bold))
        
        #Label for N(steps):
        self.label_N=QLabel(self)
        self.label_N.setText('N (number of steps)')
        self.label_N.setFixedWidth(400)
        self.label_N.setFont(QFont('Sans Serif',20))
        self.label_N.move(1600,635)

        #Textbox for global error right limit:
        self.textbox_ge=QLineEdit(self)
        self.textbox_ge.move(1465,710)
        self.textbox_ge.resize(110,50)
        self.textbox_ge.setText('100')
        self.textbox_ge.setAlignment(Qt.AlignCenter)
        self.textbox_ge.setFont(QFont('Courier New',20,QFont.Bold))
        
        #Label for global error right limit:
        self.label_ge=QLabel(self)
        self.label_ge.setText('Global Error upper bound')
        self.label_ge.setFixedWidth(400)
        self.label_ge.setFont(QFont('Sans Serif',20))
        self.label_ge.move(1595,720)

        #Create button for applying changes:
        button=QPushButton('Update',self)
        button.move(1550,800)
        button.resize(250,50)
        button.setFont(QFont('Sans Serif',16,QFont.Bold))
        button.clicked.connect(self.on_click)       #connects button to function on_click
        
        self.show()     #display the elements
    
    @pyqtSlot()     #Update graphs using new values when 'Update' button is clicked:
    def on_click(self):
        try:
            x0=int(self.textbox_x0.text())
            y0=int(self.textbox_y0.text())
            xm=int(self.textbox_X.text())
            steps=int(self.textbox_N.text())
            gemax=int(self.textbox_ge.text())
        except Exception:
            er=Error()
            sleep(2)
        else:
            if(steps>=5 and gemax>=10):
                m=PlotCanvas(self, width=14.5, height=11.5, y0=y0, x0=x0, xm=xm, steps=steps, gemax=gemax)       #main function
                m.move(-50,-75)
                m.show()
            else:
                er=Error()
                sleep(2)


class PlotCanvas(FigureCanvas):         #display main canvas:
    #initializing main canvas:
    def __init__(self, parent=None, width=14.5, height=11.5, dpi=100, x0=1, y0=1, xm=7, steps=25, gemax=100):
        fig=Figure(figsize=(width,height),dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self,QSizePolicy.Expanding,QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.plot(x0=x0,y0=y0,xm=xm,steps=steps, gemax=gemax)
    
    def plot(self, x0=1, y0=1, xm=7, steps=25, gemax=100):        #plotting graphs itselves:
        #Compute main functions:
        x_exact, y_exact=Exact(steps, xm=xm, x0=x0, y0=y0)
        x_euler, y_euler=Euler(steps, xm=xm, x0=x0, y0=y0)
        x_ie, y_ie=ImprovedEuler(steps, xm=xm, x0=x0, y0=y0)
        x_rk, y_rk=RungeKutta(steps, xm=xm, x0=x0, y0=y0)
        
        #Display the first plot with main graphs:
        ax=self.figure.add_subplot(311)     #determines the position of the box with main graphs in UI
        ax.title.set_text('Main function values:')      #add plot title
        ax.set_ylabel('y_i')
        ax.set_xlabel('x_i')
        ax.xaxis.labelpad=-3       #move x-axis label closer to the axis
        ax.plot(x_exact, y_exact, label='Exact')
        ax.plot(x_euler, y_euler, label='Euler')
        ax.plot(x_ie, y_ie, label='ImprovedEuler')
        ax.plot(x_rk, y_rk, label='Runge-Kutta')
        ax.legend()
        
        #Calculate local errors for 3 numerical methods:
        euler_lerror=[0.0]
        ie_lerror=[0.0]
        rk_lerror=[0.0]
        for i in range(steps):
            euler_lerror.append(fabs(y_exact[i] - y_euler[i]))
            ie_lerror.append(fabs(y_exact[i] - y_ie[i]))
            rk_lerror.append(fabs(y_exact[i] - y_rk[i]))
        
        #Display the second plot with local errors:
        ax=self.figure.add_subplot(312)
        ax.title.set_text('Local errors:')      #add plot title
        ax.set_ylabel('Local error')
        ax.set_xlabel('x_i')
        ax.xaxis.labelpad=-3       #move x-axis label closer to the axis
        ax.plot(x_euler, euler_lerror, label='Euler')
        ax.plot(x_ie, ie_lerror, label='ImprovedEuler')
        ax.plot(x_rk, rk_lerror, label='Runge-Kutta')
        ax.legend()
        
        #Calculate global errors for 3 numerical methods:
        gemin=5
        array=[]    #array for x axis in global error graph
        euler_gerror=[]
        ie_gerror=[]
        rk_gerror=[]
        for i in range(gemin,gemax,(gemax-gemin)//100+1):
            array.append(i)
            
            #Compute each method with 'i' accuracy:
            x_exact, y_exact=Exact(i, xm=xm, x0=x0, y0=y0)
            x_euler, y_euler = Euler(i, xm=xm, x0=x0, y0=y0)
            x_ie, y_ie = ImprovedEuler(i, xm=xm, x0=x0, y0=y0)
            x_rk, y_rk = RungeKutta(i, xm=xm, x0=x0, y0=y0)
            euler_maxerror=0
            ie_maxerror=0
            rk_maxerror=0
            for j in range(i):
                if fabs(y_exact[j] - y_euler[j]) > euler_maxerror:
                    euler_maxerror = fabs(y_exact[j] - y_euler[j])
                if fabs(y_exact[j] - y_ie[j]) > ie_maxerror:
                    ie_maxerror = fabs(y_exact[j] - y_ie[j])
                if fabs(y_exact[j] - y_rk[j]) > rk_maxerror:
                    rk_maxerror = fabs(y_exact[j] - y_rk[j])
            
            euler_gerror.append(euler_maxerror)
            ie_gerror.append(ie_maxerror)
            rk_gerror.append(rk_maxerror)
        
        #Display the third plot with global errors:
        ax=self.figure.add_subplot(313)
        ax.figure.subplots_adjust(hspace=0.3)
        ax.title.set_text('Global errors:')      #add plot title
        ax.set_ylabel('Maximum error')
        ax.set_xlabel('Number of steps')
        ax.plot(array, euler_gerror, label='Euler')
        ax.plot(array, ie_gerror, label='ImprovedEuler')
        ax.plot(array, rk_gerror, label='Runge-Kutta')
        ax.legend()
        
        self.draw()     #display all 3 graphs


class Error(QMainWindow):
    def __init__(self):     #setting Error window properties:
        super().__init__()
        self.left=1525
        self.top=847
        self.title='INPUT ERROR!!!'
        self.width=300
        self.height=31
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()
        
if __name__ == '__main__':
    #print(exactSolution(1,0,0))
    app=QApplication(sys.argv)
    ex=App()
    sys.exit(app.exec_())
